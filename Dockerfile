FROM bhgedigital/envsubst
RUN apk add --update --no-cache ca-certificates openssl bash && update-ca-certificates
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
COPY . /
RUN chmod a+x /*.sh
ENTRYPOINT ["/entrypoint.sh"]